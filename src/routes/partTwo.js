export default [
  {
    name: 'csaLawIntervention',
    question:
      'Child sexual abuse requires intervention from law enforcement to prevent further harm.',
    response: 0,
    invert: true,
    part: 1,
  },

  {
    name: 'sexOffenderRegistry',
    question: 'The sex offender registry does more harm than good.',
    response: 0,
    invert: false,
    part: 1,
  },

  {
    name: 'interventionsCPS',
    question:
      'Interventions from child protective services make the situation worse for the child.',
    response: 0,
    invert: false,
    part: 1,
  },

  {
    name: 'societyAcceptingOfChildAdultSex',
    question:
      'If society was more accepting of child-adult-sex, then it would be less harmful.',
    response: 0,
    invert: false,
    part: 1,
  },

  {
    name: 'CPHarmsChild',
    question: 'Downloading child pornography is harmful to the child in it.',
    response: 0,
    invert: true,
    part: 1,
  },

  {
    name: 'MAPTherapyPreventOffending',
    question: 'MAPs need therapy to prevent offending.',
    response: 0,
    invert: true,
    part: 1,
  },

  {
    name: 'adultsSexChildrenImmoral',
    question:
      'Adults who have sexual contact with children are immoral people.',
    response: 0,
    invert: true,
    part: 1,
  },
]
